<?php
/**
* @package mamplugin
*/

/*
Plugin Name: Mam plugin
plugin URI: http://www.affroapps.com
Description: Mwine Afrod Melchisedec pluggin
Version: 1.0.0
Author: Mwine Afrod Melchisedec
Author URI: http://www.affroapps.com
License: GPLv2 or later
Text Domain: mam-pluggin
*/
 
/*
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see < http://www.gnu.org/licenses/ >.
*/

// public - can be accessed everywhere
// protected - can be accessed only within the class itself OR a class that extends the class
// private - can be accessed only by the class itself
// static - allow calling a function in a class without initalizing the class


// If this file is called directly, abort
defined( 'ABSPATH' ) or die( 'You are not allowed to be here' );

if ( file_exists( dirname( __FILE__ ) . '/vendor/autoload.php' ) ) {
	require_once dirname( __FILE__ ) . '/vendor/autoload.php';
}




// code that runs during activation
function activate_mam_plugin() {
	Inc\Base\Activate::activate();
}

// code that runs during deactivation
function deactivate_mam_plugin() {
	Inc\Base\Deactivate::deactivate();
}

register_activation_hook( __FILE__, 'activate_mam_plugin' );
register_deactivation_hook( __FILE__, 'deactivate_mam_plugin' );


if ( class_exists( 'Inc\\Init' ) ) {
	Inc\Init::register_services();
}























